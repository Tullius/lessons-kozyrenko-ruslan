/*

  Модули в JS
  https://developer.mozilla.org/ru/docs/Web/JavaScript/Reference/Statements/import

  Так как для экспорта и импорта нету родной поддержки в
  браузерах, то нам понадобится сборщик который это умеет делать

  Выбор пал на вебпак

  npm i
  npm run cli

  Затестим - в консоли наберем команду webpack


  "webpack 
    ./application/index.js 
    --output-path ./public/js 
    --output-filename bundle.js 
    --mode development 
    --color 
    --watch"

*/

import test from './imports';
import React from  'react';
import ReactDom from 'react-dom';
console.log('WEBPACK WORKING23123!')
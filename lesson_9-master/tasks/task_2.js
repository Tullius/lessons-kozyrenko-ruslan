/*

    Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678


*/
let body = document.querySelector('body');
let form = document.createElement('form');
let login = document.createElement('input');
let pass = document.createElement('input');
let buttonLogin = document.createElement('button');
let buttonExit = document.createElement('button');
let button = document.createElement('button');
let hello = document.createElement('div');
let loginForm = document.createElement('div');

if(localStorage.getItem('login')) {
    body.appendChild(hello);
    body.appendChild(buttonExit);
    buttonExit.innerText = 'Exit';
    loginForm.innerHTML = '';
    hello.innerText = 'Привет '+localStorage.getItem('login');
    buttonExit.addEventListener('click', e => {
        localStorage.clear();
        window.location.reload();
    });
} else {
    login.type = 'text';
    pass.type = 'password';
    buttonLogin.innerText = 'Login';
    body.appendChild(loginForm);
    loginForm.appendChild(form);
    form.appendChild(login);
    form.appendChild(pass);
    form.appendChild(buttonLogin);
    buttonLogin.addEventListener('click', e => {
        localStorage.setItem('login', login.value);
        localStorage.setItem('password', pass.value);
    });
}
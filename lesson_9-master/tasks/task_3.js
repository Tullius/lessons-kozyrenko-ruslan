/*

    Задание 3:


    Написать класс Posts в котором есть данные:

    _id
    isActive,
    title,
    about,
    likes,
    created_at

    У класса должен быть метод addLike и render.

    Нужно сделать так чтобы:
    - После добавления поста, данные о нем записываются в localStorage.
    - После перезагрузки страницы, данные должны сохраниться.
    - Можно было прездагрузить данные в данный модуль: http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2

*/
window.addEventListener('load', function () {
    class Posts {
        constructor(_id, isActive, title, about, created_at, likes = 0){
            this._id = _id;
            this.isActive = isActive;
            this.title = title;
            this.about = about;
            this.created_at = created_at;
            this.likes = likes;
            this.addLike = this.addLike.bind(this);
            this.render = this.render.bind(this);
        };
        addLike(){

        };
        render(){

        };
    };
    async function getCompanies() {
        let companies = await fetch("http://www.json-generator.com/api/json/get/cgCRXqNTtu?indent=2").then(response => response.json());
        return companies;
    };
};
/*
class Posts {
    constructor(_id, isActive, title, about, likes, created_at) {
        this.__id = _id;
    }
    addLike(){

    }
    render(){

    }
};
let x = new Posts();*/
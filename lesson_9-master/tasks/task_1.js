/*

    Задание 1:
    Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
    После перезагрузки страницы, цвет должен сохранится.

*/
window.addEventListener('load', function() {
    let button = document.createElement('button');
    let body = document.querySelector('body');
    button.innerText = 'push me';
    body.appendChild(button);
    button.addEventListener('click', e => {
        let background_color = "#"+((1<<24)*Math.random()|0).toString(16);
        localStorage.setItem('color', background_color);
        body.style.backgroundColor = background_color;
    });
    body.style.backgroundColor = localStorage.getItem('color');
});
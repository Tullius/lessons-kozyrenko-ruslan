/*
  Cookies - специальный формат для сохранения данных который хранится в браузере.
  Сохраняет пары значений name-value:
  username = Vasya Pupkin

  document.cookie
  
  Браузеры имеют следующие лимиты на хранение cookies в приделах одного домена:

  Chrome has no limit on the maximum bytes per domain.
  Firefox has no limit on the maximum bytes per domain.
  Internet Explorer allows between 4,096 and 10,234 bytes.
  Opera allows 4,096 bytes.
  Safari allows 4,096 bytes

  Так же есть лимит в кол-ве -- не более чем 30 кук на 1 домен. 
  Он не имеет технических ограничений. Это общий срез по всем браузерам, о комфортном числе кук, с которыми все браузеры смогут работать.

*/
// console.log('cookies works', document.cookie);

// Такая cookie - будет сохраненна в текущей сессии, но будет удалена после закрытия вкладки
document.cookie = "itea=JsAdvanced08/2019";
document.cookie = "token=8743b52063cd84097a65d1633f5c74f5";

// for( i = 0; i< 30; i++){
//   document.cookie = `itea${i}=JS`;
// }

/*
  Enode demo:
  encodeURIComponent изменяет все символы, за исключением следующих: латинские буквы, десятичные цифры, - _ . ! ~ * ' ( )
*/

let filename = "my file name with spaces.txt";
let decode = encodeURIComponent( filename );
console.log('encoded:', decode );
/*
  Decode demo:
*/
let decoded = decodeURIComponent( decode );
console.log('decoded', decoded )

// Значение Cookie не могут содержать точку с запятой, запятые или символы разделители.
// Перед сохранением значение в cookie желательно вызвать функцию encodeURIComponent()
// при чтении значения надо вызвать decodeURIComponent()
// записанный таким способом cookie сохраняются в текущем сеансе браузера
// но удаляются после его закрытия.

// следующий cookie будет хранится браузером на проятжении 1 недели (60 * 60 * 24 * 7).

// document.cookie = "info=123;";
// document.cookie = `info=helloJavaScript;max-age=${60 * 60 * 24 * 7};`;

// console.log( document.cookie );
// Установить дату на 1 день + от текущего момента.
// var date = new Date();
//   console.log('date', date )
//   date = date.setDate( date.getDate() + 1 );
//   console.log('date2', date );

//actors
// document.cookie = `Hello=JavaScript;path=/page`;

// var date = new Date( new Date().getTime() + (60 * 1000) );
// document.cookie = "name=value; path=/page";
// toUTCString() преобразует дату в строку, используя часовой пояс UTC.

// // Удаление куки:
// var date = new Date(0);
// document.cookie = "token=; path=/; expires=" + date;

// var cookies = document.cookie.split(';');
// console.log( document.cookie );
// console.log('- - - - -');
// fetch('/', { credentials: "same-origin"});


function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }
  value = encodeURIComponent(value);

  let updatedCookie = `${name}=${value}`;
  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }
  document.cookie = updatedCookie;
}

// function deleteCookie(name) {
//   setCookie(name, "", {
//     expires: -1
//   });
// }

// setCookie(
//   'testpath',
//   '13123',
//   {
//     expires: 127812398
//   });
deleteCookie('token')

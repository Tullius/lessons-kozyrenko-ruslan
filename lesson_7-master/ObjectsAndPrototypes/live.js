document.addEventListener('DOMContentLoaded', () => {


    // function Car( name ){
    //     let speed = 200;
    //     const changeName = ( newName ) => {
    //         console.log( this );
    //         this.name = newName;
    //     }
    //     this.name = name;
    //     this.public = function(){
    //         changeName('Jimny');
    //         console.log('public fn', this );
    //     }
    //     this.changeSpeed = function( new_speed ){
    //         speed = new_speed;
    //         console.log( speed );
    //     }   
    // }

    // let suzuki = new Car('Vitara');
    //     suzuki.public();

    //     suzuki.changeSpeed(100);
    //     suzuki.changeSpeed(200);
    //     suzuki.changeSpeed(50);
    //     console.log( suzuki );

    // function Robot( name, speed ){
    //     this.name = name;
    //     this.speed = speed;
    // }

    // Robot.prototype.move = function( addedSpeed ){
    //     this.speed += addedSpeed;
    //     console.log(`${this.name} moving with speed ${this.speed}`);
    // }

    // Robot.prototype.stop = function(){
    //     this.speed = 0;
    //     console.log(`Robot ${this.name} stoped`);
    // }

    // let r2d2 = new Robot('R2D2', 0);
    //     r2d2.move(20);
    //     r2d2.move(40);
    //     r2d2.stop();
    // console.log( r2d2 );

    // function TranslateRobot( name, speed, langs ){
    //     Robot.call( this, name, speed );
    //     this.langs = langs;
    // }   

    // TranslateRobot.prototype = Object.create( Robot.prototype );
    // TranslateRobot.prototype.move = function( addedSpeed ){
    //     this.speed += (addedSpeed/2);
    //     console.log(`${this.name} moving with speed ${this.speed}`);
    // }

    // TranslateRobot.prototype.translate = function( word ){
    //     console.log( word.split('').reverse().join('') );
    // }

    // let C3P0 = new TranslateRobot('C3P0', 0, ['uk', 'en', 'jp']);
    //     C3P0.move( 40 );
    //     C3P0.translate('Селедка')
    // console.log( C3P0 );

    let counter = 0;

    class Robot {
        constructor( name, speed ){
            this.id = counter;
            this.name = name;
            this.speed = speed;
            this.like = 0;
            RobotStore.push( this );

            counter++;
        }

        static compareRobots( robot1, robot2 ){
            console.log( robot1, robot2 );
        }

        move( addedSpeed ){
            return (e) => {
                console.log( 'arg', addedSpeed, 'event', e);
                this.speed += addedSpeed;
                console.log(`${this.name} moving with speed ${this.speed}`);
            }
        }

        stop(){
            return (e) => {
                this.speed = 0;
                console.log(`Robot ${this.name} stoped`);
            }
        }

        addLike() {
            return ( e ) => {
                    this.like++;
                    // e.target.innerHTML = `Like ${this.like}`;

                    this.render();

                    console.log('added like:', this, e.target  );
            }
        }

        render( target = document.getElementById('root') ){

            let node;
            const isOnPage =  document.querySelector(`.robot[data-id="${this.id}"`);

            if( isOnPage === null ){
                node = document.createElement('div');
                node.className = 'robot';
                node.dataset.id = this.id;
                node.innerHTML = `
                    <header>
                        <h1>${this.name}</h1>
                        <span> Likes: ${this.like} </span>
                    </header>
                    <section>
                        <button class="_move"> Move </button>
                        <button class="_stop"> Stop </button>
                        <button class="_like"> Like </button>
                    </section>
                `;

                node.querySelector('._move').addEventListener('click', this.move(20) );
                node.querySelector('._stop').addEventListener('click', this.stop() );
                node.querySelector('._like').addEventListener('click', this.addLike() );

                target.appendChild( node );
            } else {
                node = isOnPage;
                let changeArea = node.querySelector('header');
                changeArea.innerHTML = `
                    <h1>${this.name}</h1>
                    <span> Likes: ${this.like} </span>
                `;
            }

        }
    }

    // let R2D2 = new Robot('R2D2', 0);
    //     R2D2.move(20);
    //     R2D2.stop();
    // console.log( R2D2 );

    class TranslateRobot extends Robot{
        constructor( name, speed, langs ){
            super( name, speed );
            this.langs = langs;
        }
        translate(word){
            console.log( word.split('').reverse().join('') );
        }

        // render( target = document.getElementById('root') ){

        //     let node =  document.createElement('div');
        //         node.innerHTML = `
        //             <h2>${this.name}</h2>
        //             <button class="_like"> Like ${this.like} </button>
        //         `
        //         node.querySelector('._like').addEventListener('click', this.addLike() );

        //         target.appendChild( node );
        // }
    }

    // let C3P0 = new TranslateRobot('C3P0', 0, ['uk', 'en', 'jp']);
    // C3P0.move(20);
    // C3P0.stop();
        // C3P0.translate('Селедка')
        
    // TranslateRobot.prototype.material = 'steel';
    // TranslateRobot.prototype.doSmsng = function(){
    //     console.log('123');
    // }
    
    // C3P0.doSmsng();
        
    // console.log( C3P0.material );

    // Robot.compareRobots( R2D2, C3P0 );

    // const type =  typeof( Robot );
    // console.log( type );

    /*
        HOF
        
    */

    // const handler = ( arg ) => ( event ) => {
    //     if( arg ){
    //         console.log( arg, event );
    //     } else  {
    //         console.error('sorry :(');
    //     }
    // } 

    // HOF.addEventListener('click', handler( false ) );
    
    /*
        RENDER AREA:
    */

    // const HOF = document.getElementById('test');
    // const RobotStore = [];

    // let R2D2 = new Robot('R2D2', 0);
    // let C3P0 = new TranslateRobot('C3P0', 0, ['uk', 'en', 'jp']);

    // console.log( RobotStore );

    // RobotStore.map( robot => robot.render() );

});
/*
    setTimeout - метод выполняет код(или функцию),
    указанный в первом аргументе, асинхронно, с задержкой в delay миллисекунд.
    setTimeout выполняет код только один раз
*/  


document.addEventListener('DOMContentLoaded', function(){

    /*
        Syntax: setTimeout( function(){}, timeout );
    */
    // setTimeout( myFunction, 1000 );
    let timer = null;
    function myFunction() {
        console.log("Hello 1");
        timer = setTimeout(
            function(){console.log("Hello 2")},
        3000);
        console.log('Hello 3')
    }
    
    const fireBtn = document.getElementById('fire');
    fireBtn.addEventListener('click', myFunction);
    
    
    function stopTimer(){
        console.log( timer );
        clearTimeout( timer );
    }

    const stopBtn = document.getElementById('stop');
    stopBtn.addEventListener('click', stopTimer)
});

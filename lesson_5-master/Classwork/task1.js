/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/
let train = {
    name: 'Intercity',
    speed: 140,
    passenger: 1200,
    drive: function () {
        this.speed = 260;
        console.log('Поезд ' + this.name + ' везет ' + this.passenger + ' со скоростью ' + this.speed);
    },
    stend: function () {
        this.speed = 0;
        console.log('Поезд ' + this.name + ' остановился. ' + 'Скорость ' + this.speed);
    },
    pickUp: function (plusPassenger) {
        this.passenger += plusPassenger;
        console.log(this.passenger);
    },
};

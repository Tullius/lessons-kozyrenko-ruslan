/*

  Задание "Шифр цезаря":

    https://uk.wikipedia.org/wiki/%D0%A8%D0%B8%D1%84%D1%80_%D0%A6%D0%B5%D0%B7%D0%B0%D1%80%D1%8F

    Написать функцию, которая будет принимать в себя слово и количество
    симовлов на которые нужно сделать сдвиг внутри.

    Написать функцию дешефратор которая вернет слово в изначальный вид.

    Сделать статичные функции используя bind и метод частичного
    вызова функции (каррирования), которая будет создавать и дешефровать
    слова с статично забитым шагом от одного до 5.


    Например:
      encryptCesar( 3, 'Word');
      encryptCesar1(...)
      ...
      encryptCesar5(...)

      decryptCesar1(3, 'Sdwq');
      decryptCesar1(...)
      ...
      decryptCesar5(...)

      "а".charCodeAt(); // 1072
      String.fromCharCode(189, 43, 190, 61) // ½+¾

*/



function encryptCesar(shiftNumber, word) {
        let wordArr = word.split("");
        let decryptWord = ''
        for(let i = 0; wordArr.length > i; i++ ){
            decryptWord += String.fromCharCode(wordArr[i].charCodeAt() + shiftNumber);
        }
        return decryptWord;
};

var encryptCesar1 = encryptCesar.bind(null, 1);
var encryptCesar2 = encryptCesar.bind(null, 2);
var encryptCesar3 = encryptCesar.bind(null, 3);
var decryptCesar1 = encryptCesar.bind(null, -1);
var decryptCesar2 = encryptCesar.bind(null, -2);
var decryptCesar3 = encryptCesar.bind(null, -3);
var st1 = encryptCesar1('test');
var st2 = encryptCesar2('test');
var st3 = encryptCesar3('test');
console.log(st1,st2,st3);
st1 = decryptCesar1(st1);
st2 = decryptCesar2(st2);
st3 = decryptCesar3(st3);
console.log(st1,st2,st3);

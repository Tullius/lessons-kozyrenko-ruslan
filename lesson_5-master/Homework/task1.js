/*

  Задание:


    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }

      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>
*/



var proto = {
    avatarUrl: 'https://leadsbridge.com/wp-content/themes/leadsbridge/pagebuilder/assets/default_avatar.png',
    addLike: function(){
        this.likes++;
    },
};
let commentsArrey = [];
function Comment(name, text, avatarUrl) {
    this.name = name;
    this.text = text;
    if(typeof(avatarUrl) != 'undefined'){
        this.avatarUrl = avatarUrl;
    }
    this.likes = 0;
    Object.setPrototypeOf(this, proto);
    commentsArrey.push(this);
}
let myComment1 = new Comment('Natasha', 'asdasd');
let myComment2 = new Comment('vika', 'asdasd', 'https://icon-library.net/images/icon-avatars/icon-avatars-12.jpg');
let myComment3 = new Comment('sasha', 'asdasd');
let myComment4 = new Comment('bodya', 'asdasd');

function bildComments(commentsArrey){
    let div = document.createElement('div');
    div.id = 'CommentsFeed';
    document.body.appendChild(div);
    commentsArrey.forEach(function(el) {
        let button = document.createElement('button');
        let comment = document.createElement('div');
        let name = document.createElement('div');
        let text = document.createElement('div');
        let imgDiv = document.createElement('div');
        let img = document.createElement('img');
        comment.classList.add('comment');
        name.innerText = el.name;
        text.innerText = el.text;
        imgDiv.appendChild(img);
        img.src = el.avatarUrl;
        button.innerText = 'Like';
        button.addEventListener('click', function () {
            el.addLike();
            button.innerText = 'Like '+el.likes;
            console.log(el)
        });
        comment.appendChild(name);
        comment.appendChild(text);
        comment.appendChild(imgDiv);
        comment.appendChild(button);
        div.appendChild(comment);
    });
};

var express = require('express');
var cors = require('cors')
var app = express();

const PORT = 3122;

app.use( cors() );
app.use( express.static('public') );

app.get('/', function (req, res) {
  res.send('/public/index.html');
});

app.get('/data', function( req, res){
  res.json([
    { id: 1 }, { id: 2 }
  ])
})


app.post('/progress', function( req, res ){
  res.json({
    status: true
  })
})

app.listen(3122, function () {
  console.log(`Example app listening on port ${PORT}!`);
});

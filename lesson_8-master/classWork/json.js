
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

  <form>
    <input name="name" />
    <input name="age"/>
    <input name="password"/>
    <button></button>
  </form>

  <form>
    <input />
    <button></button>
  </form>
  -> '{"name" : "23123", "age": 15, "password": "*****" }'


*/
window.addEventListener('load', e => {

    let data = {};
    let body = document.querySelector('body');
    let form = document.createElement('form');
    let form2 = document.createElement('form');
    let firstName = document.createElement('input');
    let age = document.createElement('input');
    let password = document.createElement('input');
    let button = document.createElement('button');
    let jsonParse = document.createElement('input');
    let button2 = document.createElement('button');
    firstName.placeholder = 'Введите имя';
    age.placeholder = 'Введите возраст';
    password.placeholder = 'Введите пароль';
    jsonParse.placeholder = 'Введите строку JSON.parse';
    jsonParse.type = 'text';
    firstName.type = 'name';
    age.type = 'number';
    password.type = 'password';
    button.innerText = 'Push me';
    button2.innerText = 'Push me please';
    form.appendChild(firstName);
    form.appendChild(age);
    form.appendChild(password);
    form.appendChild(button);
    form2.appendChild(jsonParse);
    form2.appendChild(button2);
    body.appendChild(form);
    body.appendChild(form2);
    button.addEventListener('click', e => {
        e.preventDefault();
        data = {
            name: firstName.value,
            age: age.value,
            password: password.value,
        };
        console.log(JSON.stringify(data));
    });
    button2.addEventListener('click', e => {
        e.preventDefault();
        let newData = jsonParse.value;
        newData = JSON.parse(newData);
        console.log(Array.from([newData.name,newData.age,newData.password]));

    });
});
/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    # | Company  | Balance | Показать дату регистрации | Показать адресс |
    1.| CompName | 2000$   | button                    | button
    2.| CompName | 2000$   | 20/10/2019                | button
    3.| CompName | 2000$   | button                    | button
    4.| CompName | 2000$   | button                    | button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/
window.addEventListener('load', function () {

    async function getCompanies() {
        let companies = await fetch("http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2").then(response => response.json());
        return companies;
    };
    async function render() {
        let companies = await getCompanies();
        let body = document.querySelector('body');
        body.innerHTML = `
    <table>
    <tr>
        <th>#</th>
        <th>Company</th>
        <th>Balance</th>
        <th>Показать дату регистрации</th>
        <th>Показать адресс</th>
    </tr>
</table>
    `;
        let table = document.querySelector('table');
        companies.forEach(function (item) {
            let tr = document.createElement('tr');
            let id = document.createElement('td');
            let company = document.createElement('td');
            let balance = document.createElement('td');
            let date = document.createElement('td');
            let adress = document.createElement('td');
            let dateButton = document.createElement('button');
            let adressButton = document.createElement('button');
            id.innerText = item._id;
            company.innerText = item.company;
            balance.innerText = item.balance;
            tr.appendChild(id);
            tr.appendChild(company);
            tr.appendChild(balance);
            tr.appendChild(date);
            tr.appendChild(adress);
            dateButton.innerText = 'Show date';
            adressButton.innerText = 'Show adress';
            dateButton.addEventListener('click', e => {
                date.innerHTML = item.registered;
            });
            adressButton.addEventListener('click', e => {
                adress.innerHTML = `${item.address.city} ${item.address.country} ${item.address.house} ${item.address.state}`;
            });
            date.appendChild(dateButton);
            adress.appendChild(adressButton);
            table.appendChild(tr);
            console.log(item._id);
        });
        console.log(companies);
    };
    render();
});
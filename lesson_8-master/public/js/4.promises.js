document.addEventListener('DOMContentLoaded', () => {

    /*

    */  

    // let promiseObj = new Promise( ( resolve, reject) => {
    //     console.log('In promisee')  
    //     ///...
    //     // resolve([
    //     //     {
    //     //         id: 1,
    //     //         name: 'Dexter'
    //     //     },
    //     //     {
    //     //         id: 2,
    //     //         name: 'John'
    //     //     }
    //     // ]);
    //     reject( null ); 
    // })
    //     .then( 
    //         ( success ) => {
    //             console.log('success', success);
    //             return success;
    //         },
    //         ( error ) => {
    //             console.log('error', error);
    //             return [
    //                 {
    //                     id: 3,
    //                     name: 'Cached'
    //                 }
    //             ]
    //         }
    //     )
    //     //..
    //     .then( res => {
    //         return res.map( item => {
    //             item.color = 'red';
    //             return item;
    //         });
    //     })
    //     //..
    //     .then( 
    //         ( res ) => {
    //             console.log('response', res );
    //             res.map( item => console.log( item ));
    //         }
    //     )
    //     .catch( error => {
    //         console.error('We have a problem', error );
    //     })
    // console.log( promiseObj  );

    let RenderImage = (image) => document.getElementById("target").appendChild(image);

    const loadImagePromise = ( url ) => 
        new Promise( (resolve, reject) => {
            
            let imageElement = new Image();
            imageElement.onload = function(){
                resolve(imageElement);
            };

            imageElement.onerror = function(){
                var message = 'Error on image load at url ' + url;
                imageElement.src = 'images/cat5.jpg';
                reject(imageElement);
            };
            
            imageElement.src = url;
        });


    // loadImagePromise('images/cat1.jpg')
    //     .then( RenderImage )
    //     .catch( RenderImage )


    Promise.all([
        loadImagePromise('images/cat1.jpg'),
        loadImagePromise('images/cat2.jpg'),
        loadImagePromise('images/cat3.jpg'),
        loadImagePromise('images/cat4.jpg'),
        loadImagePromise('images/cat6.jpg')
    ])
    .then( res => {
        console.log( res );
        res.map( RenderImage )
    })

});
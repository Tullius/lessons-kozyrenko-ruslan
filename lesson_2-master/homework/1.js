
  /*

    Задание 1.

    Написать скрипт который будет будет переключать вкладки по нажатию
    на кнопки в хедере.

    Главное условие - изменять файл HTML нельзя.

    Алгоритм:
      1. Выбрать каждую кнопку в шапке
        + бонус выбрать одним селектором

      2. Повесить кнопку онклик
          button1.onclick = function(event) {

          }
          + бонус: один обработчик на все три кнопки

      3. Написать функцию которая выбирает соответствующую вкладку
        и добавляет к ней класс active

      4. Написать функцию hideAllTabs которая прячет все вкладки.
        Удаляя класс active со всех вкладок

    Методы для работы:

      getElementById
      querySelector
      classList
      classList.add
      forEach
      onclick

      element.onclick = function(event) {
        // do stuff ...
      }

  */

  if(typeof(init) == 'undefined') {
      var init = true;
      window.addEventListener("load", function () {
          let buttons = document.getElementById('buttonContainer').querySelectorAll('button');
          let tabs = document.getElementById('tabContainer').querySelectorAll('.tab');
          console.log(tabContainer);
          buttons.forEach( function( selectBtn ){
              selectBtn.onclick = function(event) {
                tabs.forEach(function( el ){
                    el.classList.remove('active');
                        if(event.target.dataset.tab == el.dataset.tab) {
                            el.classList.add('active');
                        };
                });
              }
          });
      });
  };


